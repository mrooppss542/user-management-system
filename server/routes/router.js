const express = require('express');
const route = express.Router(); //This is a express method we can be used to create routes in different folder and exports the routes.
const services = require('../services/render');
const controller = require('../controller/control');
/*
route.get('/',(err,res)=>{
    //req.send("Crud Application");
    res.render("index");
})

route.get('/addtask',(err,res)=>{
    //req.send("Crud Application");
    res.render('addtask');
})

route.get('/updatetask',(err,res)=>{
    //req.send("Crud Application");
    res.render('updatetask');
})
*/
//Using services for routing

/**
 * @description root route
 * @method GET/
 */
route.get('/',services.homeroutes);

/**
 * @description Add task route
 * @method GET/addtask
 */
route.get('/addtask',services.addroutes);

/**
 * @description Update task route
 * @method GET/updatetask
 */
route.get('/updatetask',services.updateroutes);

/**
 * @API route
 */
route.post('/api/tasks',controller.create);
route.get('/api/tasks',controller.find);
route.put('/api/tasks/:id',controller.update); //updates via id,
route.delete('/api/tasks/:id',controller.delete);
//export routes
module.exports=route;
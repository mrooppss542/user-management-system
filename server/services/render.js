const axios = require('axios');


exports.homeroutes = (err,res)=>{
    //Make a get request to /api/tasks
    //res.render("index");
    axios.get("http://localhost:1000/api/tasks")
    .then(response=>{
        //console.log(response)
        res.render("index",{tasks:response.data});
    })
    .catch(err=>{
        res.send(err)
    })
}
exports.addroutes=(err,res)=>{
    res.render('addtask');
}
exports.updateroutes=(req,res)=>{
    axios.get('http://localhost:1000/api/tasks',{params:{id:req.query.id}})
    //res.render('updatetask');
    .then(taskdata =>{
        res.render("updatetask",{task:taskdata.data})
    })
    .catch(err=>{
        res.send(err);
    })
}
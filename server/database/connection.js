const mongoose = require('mongoose');
const connectdb = async()=>{
    try{
//mongodb connection string.
        const con = await mongoose.connect(process.env.MONGO_URL,{
            useNewUrlParser:true,
            useUnifiedTopology:true,
            useCreateIndex:true,
            useFindAndModify:false
        })
        console.log(`MongoDB connected : ${con.connection.host}`);
    }
    catch(e){
        console.log(e);
        process.exit(1);
    }
}

module.exports=connectdb;
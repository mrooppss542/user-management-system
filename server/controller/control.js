var taskdb = require("../model/model");

//Create and save new tasks.

exports.create=(req,res)=>{
    //validate request.
    if(!req.body){
        res.status(400).send({message : "Content cannot be empty"});
        return;
    }

    //new Task
    const task = new taskdb({
        name:req.body.name,
        taskname:req.body.taskname,
        description:req.body.description,
        status:req.body.status,
        date:req.body.date
    })
    //save task in database.
    task
        .save(task)
        .then(data=>{
            //res.send(data)
            res.redirect('/addtasks')
    })
        .catch(err=>{
        res.status(500).send({
            message:err.message || "some error occured while created a create operation"
        });
    });

}

//retreive and return values of a single task.
exports.find=(req,res)=>{
    if(req.query.id){
        const id=req.query.id;
        taskdb.findById(id)
        .then(data=>{
            if(!data){
                res.status(404).send({message:"Not found user with id"});
            }
            else{
                res.send(data);
            }
        })
        .catch(err=>{
            res.status(500).send({message:"Error retrieving user with id"});
        })
    }
    else{
        taskdb.find()
        .then(task =>{
            res.send(task)
    })
    .catch(err=>{
        res.status(500).send({message:err.message || "Error encountered"});
    })
    }
}

//update task
exports.update=(req,res)=>{
    if(!req.body){
        res.status(400).send({message : "Content cannot be empty"});
        return;
    }
    const id=req.params.id;
    taskdb.findByIdAndUpdate(id,req.body,{useFindAndModify:false})
    .then (data =>{
        if(!data){
            res.status(404).send({message:`cannot update task with ${id}.May be no task is found`})
        }
        else{
            res.send(data);
        }
    })
    .catch(err=>{
        res.status(500).send({message:'Error encountered'})
    })
}

//delete task
exports.delete=(req,res)=>{
    if(!req.body){
        res.status(400).send({message : "Content cannot be empty"});
        return;
    }
    const id=req.params.id;
    taskdb.findByIdAndDelete(id)
    .then (data =>{
        if(!data){
            res.status(404).send({message:`cannot update task with ${id}.May be no task is found`})
        }
        else{
            res.send({message : "Task deleted sucessfully"});
        }
    })
    .catch(err=>{
        res.status(500).send({message:'Error encountered'})
    })
}
//Creating mongoDb schema

const mongoose = require('mongoose');
var schema = new mongoose.Schema({
    name:{
        type:String,
        required:true
    },
    date:{
        type:String,
        required:true,
    },
    taskname:{
        type:String,
        required:true
    },
    description:{
        type:String,
        required:true
    },
    status:{
        type:String,
        required:true
    }
    }
);
//Allows us to create shape and content of the document.

const taskdb=mongoose.model('taskdb',schema);

module.exports=taskdb;


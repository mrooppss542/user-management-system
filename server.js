const express = require('express');
const bodyparser = require("body-parser");
const app = express();
const morgan = require('morgan');
const path=require('path');
const connectDB = require('./server/database/connection');
const dotenv = require('dotenv');
dotenv.config({path:'config.env'});
//log requests
app.use(morgan('tiny'));

//MongoDb connection
connectDB();

//parse request to body parser.
app.use(bodyparser.urlencoded({extended : true}));
app.set("view engine","ejs");
app.set("views",path.resolve(__dirname,"views"));

//load assets
app.use('/css',express.static(path.resolve(__dirname,"assets/css")));//we can specify like css/style.css
app.use('/images',express.static(path.resolve(__dirname,"assets/images")));
app.use('/js',express.static(path.resolve(__dirname,"assets/js")));

app.use('/',require('./server/routes/router'));
app.listen(1000,()=>{
    console.log(`server is running on http://localhost:${1000}`);
})